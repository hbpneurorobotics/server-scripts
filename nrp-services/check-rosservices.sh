#!/bin/sh

log() { echo "$(date): $*"; }

source /opt/bbp/ros/rosenv.sh

function isRosservicesOk(){
  if ! rosservice list 2>/dev/null | grep -q "/ros_cle_simulation/create_new_simulation" ; then
    return 1
  fi
}

log "Checking ROS services..."

if  ! isRosservicesOk ; then
  log "ROS services not OK... retrying in 15s"
  sleep 15
  if ! isRosservicesOk ; then
    log "ROS services not OK. Restarting supervisor control services..."
    supervisorctl restart all
    exit $?
  fi
fi

log "ROS services looking OK"
