#!/bin/sh

# Loads all the modules and set all needed environment variables
DIR="$( cd "$( dirname "$0" )" && pwd )"
. $DIR/nrp-services-env.sh

# Tells ROS controllers to sync with Gazebo by setting rosparam use_sim_time to 1.
# But before, wait 10 seconds maximum for roscore (in case roscore was started just before).
timeout 10 rostopic list
retval=$?
# timeout. If the command times out, then exit with status 124.
if [ $retval -eq 124 ]; then
        >&2 echo "roscore is not running. Can't start the ROSCLESimulation factory."
        exit 2 # Using 2 since it's defined as a definitive exit status in supervisor
fi
rosparam set use_sim_time 1

# Use exec here so that supervisord can start AND stop the process
exec python $NRP_MODULE_HOME/lib/python2.7/site-packages/hbp_nrp_cleserver/server/ROSCLESimulationFactory.py
