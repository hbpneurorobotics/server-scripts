#!/bin/sh
#
# gzbridge Communication bridge for accessing Gazebo via the web
#
# description: Handles the gzbridge component which bridges the communication \
#              between Gazebo (protobuf) and the web (JSON)

# Source function library.
. /etc/rc.d/init.d/functions

# Puppet hiera variables
. /opt/bbp/nrp-variables

GAZEBO_VERSION=${GAZEBO_VERSION:-last-build}
SDF_VERSION=${SDF_VERSION:-last-build}
SIMBODY_VERSION=${SIMBODY_VERSION:-last-build}
OPENSIM_VERSION=${OPENSIM_VERSION:-last-build}

export MODULEPATH=$MODULEPATH:/gpfs/bbp.cscs.ch/apps/viz/neurorobotics/modulefiles
source /gpfs/bbp.cscs.ch/apps/viz/set_module_path.sh
module load boost/1.55zlib-rhel6-x86_64-gcc4.4
module load sdf/$SDF_VERSION
module load simbody/$SIMBODY_VERSION
module load opensim/$OPENSIM_VERSION
module load gazebo/$GAZEBO_VERSION
module load gzweb/last-build
module load ogre/1.9.0-rhel6-x86_64-gcc-4.8.2

if [ -z $GZBRIDGE_BIN_DIR ]; then
    echo "Necessary environment variable GZBRIDGE_BIN_DIR not set!"
    exit 1
fi

exec="$GZBRIDGE_BIN_DIR/ws_server.js"
prog="node"

lockfile=/$HOME/lock/$prog

start() {
    echo -n $"Starting $prog: "

    # we have to change to the directory, otherwise loading of materials
    # does not work properly :(
    cd $GZBRIDGE_BIN_DIR

    # if not running, start gzbridge up here
    $prog $exec &
    retval=$?
    GZBRIDGE_PID=$!
    if [ $retval -eq 0 ]; then
        echo_success
    fi
    echo
    [ $retval -eq 0 ] && echo $GZBRIDGE_PID > $lockfile
    return $retval
}

stop() {
    echo -n $"Stopping $prog: "
    # stop gzserver here with the help of the kill function
    kill -9 $(<$lockfile)
    retval=$?
    echo
    [ $retval -eq 0 ] && rm -f $lockfile
    return $retval
}

restart() {
    stop
    start
}

reload() {
    restart
}

force_reload() {
    restart
}

rh_status() {
    # run checks to determine if the service is running or use generic status
    status $prog
}

rh_status_q() {
    rh_status >/dev/null 2>&1
}


case "$1" in
    start)
        rh_status_q && exit 0
        $1
        ;;
    stop)
        rh_status_q || exit 0
        $1
        ;;
    restart)
        $1
        ;;
    reload)
        rh_status_q || exit 7
        $1
        ;;
    force-reload)
        force_reload
        ;;
    status)
        rh_status
        ;;
    condrestart|try-restart)
        rh_status_q || exit 0
        restart
        ;;
    *)
        echo $"Usage: $0 {start|stop|status|restart|condrestart|try-restart|reload|force-reload}"
        exit 2
esac
exit $?
