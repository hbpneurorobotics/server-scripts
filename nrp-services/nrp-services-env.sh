#!/bin/sh

# Puppet hiera variables
. /opt/bbp/nrp-variables

# source environment modules init file
. /usr/share/Modules/init/bash 2> /dev/null

# Unfortunately, supervisorctl does not set the $USER or $HOME variables.
export HOME=/home/$NRP_USER
export USER=$NRP_USER

# Configure terminal type so ssh sessions don't use incorect encoding types
export TERM=linux

# Enable python 2.7 and activate virtualenv
source /opt/rh/python27/enable
source $NRP_MODULE_HOME/bin/activate

# Loads all the modules and related configuration
DIR="$( cd "$( dirname "$0" )" && pwd )"
. $DIR/nrp-services-modules.sh

# Frontend resource path
export ESVRENDER_MATERIAL_PATH=$GAZEBO_RESOURCE_PATH

# Models and experiments paths
export NRP_MODELS_DIRECTORY=/opt/hbp/gazebo/models
export NRP_EXPERIMENTS_DIRECTORY=/opt/hbp/gazebo/models

# nrp-services configuration
export PYTHONPATH=$PYTHONPATH:/opt/bbp/nrp-services/nrp-services_venv/lib/python2.7/site-packages/hbp_nrp_backend
export APP_SETTINGS=config.DeploymentConfig
