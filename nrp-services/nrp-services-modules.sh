#!/bin/sh

# ensure gpfs is mounted and modules are reachable
NRP_MODULEPATH=/gpfs/bbp.cscs.ch/apps/viz/neurorobotics/modulefiles
if [ ! -d "$NRP_MODULEPATH" ] ; then
    echo "NRP modules directory does not exist, gpfs may not be mounted. Aborting!"
    exit -1
fi

# configure modules path
export MODULEPATH=$MODULEPATH:$NRP_MODULEPATH
source /gpfs/bbp.cscs.ch/apps/viz/set_module_path.sh

GAZEBO_VERSION=${GAZEBO_VERSION:-last-build}
SDF_VERSION=${SDF_VERSION:-last-build}
ROS_HBP_PACKAGES_VERSION=${ROS_HBP_PACKAGES_VERSION:-last-build}
RETINA_VERSION=${RETINA_VERSION:-last-build}
NEST_VERSION=${NEST_VERSION:-last-build}
MVAPICH_VERSION=${MVAPICH_VERSION:-last-build}
SIMBODY_VERSION=${SIMBODY_VERSION:-last-build}
OPENSIM_VERSION=${OPENSIM_VERSION:-last-build}
NUMPY_VERSION=1.11

function check_module_load {

    # argument is the name of the module to load
    if [ "$#" -ne 1 ]; then
        echo "check_module_load takes exactly one argument. Aborting!"
        exit -1
    fi

    # try to load the module, the return code is always 0 even for an error
    module load $1

    # check the module list to make the exact version was actually loaded
    module list 2>&1 | grep -q "$1"
    if [ $? -ne "0" ] ; then
        echo "Unable to load module $1. Aborting!"
        exit -1
    fi
}

check_module_load boost/1.55zlib-rhel6-x86_64-gcc4.4
check_module_load ros/$ENVIRONMENT/indigo-numpy-$NUMPY_VERSION-rhel6-x86_64-gcc4.8.2
check_module_load ros-thirdparty/$ENVIRONMENT/indigo-numpy-$NUMPY_VERSION-rhel6-x86_64-gcc4.8.2
check_module_load ros-hbp-packages/$ROS_HBP_PACKAGES_VERSION
check_module_load sdf/$SDF_VERSION
check_module_load simbody/$SIMBODY_VERSION
check_module_load opensim/$OPENSIM_VERSION
check_module_load gazebo/$GAZEBO_VERSION
check_module_load opencv/2.4.9-numpy-$NUMPY_VERSION-rhel6-x86_64-gcc-4.4.7
check_module_load ogre/1.9.0-rhel6-x86_64-gcc-4.8.2
check_module_load tbb/4.0.5-rhel6-x86_64-gcc4.4
check_module_load nest/$NEST_VERSION
check_module_load mvapich2/$MVAPICH_VERSION
check_module_load console_bridge/0.2.7-rhel6-x86_64-gcc-4.8.2
check_module_load urdf/0.3.0-rhel6-x86_64-gcc-4.8.2
check_module_load collada-dom/2.3.0-rhel6-x86_64-gcc-4.8.2
check_module_load retina/$RETINA_VERSION

# Looks like it is better to set this variable. The awk part takes the first IP (we
# have several of them on Lugano servers).
export ROS_IP=`hostname -I | awk '{print $1}'`

source $ROS_SETUP_FILE
source $GAZEBO_RESOURCE_PATH/setup.sh
source $ROS_THIRDPARTY_PACKAGES_SETUP_FILE
source $ROS_HBP_PACKAGES_SETUP_FILE

# successfully loaded all modules, provide output for pexpect based scripts
echo "NRP modules loaded."
