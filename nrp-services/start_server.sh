#!/bin/sh

# Loads all the modules and set all needed environment variables
DIR="$( cd "$( dirname "$0" )" && pwd )"
. $DIR/nrp-services-env.sh

# Use exec here so that supervisord can start AND stop the process
exec uwsgi --ini $SERVICE_HOME/uwsgi-$SERVICE_NAME.conf --pyargv "--port $SERVICE_PORT"
