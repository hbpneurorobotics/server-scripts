#!/bin/bash

# This script builds an RPM containing all the server script we are using for the nrp-services.
# Author: Daniel Peppicell (with lot of code taken from Stefan Deser)


# First fetch the names from the respective .spec files
NEUROROBOTICS_SERVER_SCRIPTS_PKG_NAME=$(grep Name hbp-neurorobotics-server-scripts.spec | awk '{print $2}')
NEUROROBOTICS_ROS_SCRIPTS_PKG_NAME=$(grep Name hbp-neurorobotics-ros-scripts.spec | awk '{print $2}')

BUILD_DIR=neurorobotics-server-scripts-pkg
RPM_TARGET_DIR=target

# The RPM package repositories
TESTING_REPOSITORY=neurorobotics-testing
RELEASE_REPOSITORY=neurorobotics

# Filter the arguments for the script. Also see:
# http://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash
#
# We do initialize the values with "0" and only in case the respective argument is
# passed on the commandline we set it to "1". This can the later be used in order
# to react accordingly (e.g. upload the packages to a remote repository).
RELEASE=0

while [[ $# > 0 ]]
do
key="$1"

case $key in
    --release)
    RELEASE=1
    ;;
    *)
    # unknown option, ignore
    ;;
esac
shift # past argument or value
done


function log() {
    echo "[HBP-NRP] $(date --rfc-3339=seconds) $*"
}

function clean_up() {
    log "Cleaning up (old) build files"
    rm -rf ./$BUILD_DIR
    rm -rf ./$RPM_TARGET_DIR
}

# Builds the package specified as argument and cleans up the build directory
# afterwards.
function build_rpm_package() {
    local package_name=$1
    log "Building $package_name package now ..."

    log "Checking if .spec file exists"
    if [ ! -f $package_name.spec ]; then
      log "The .spec file for $package_name does not exist! Exiting now!"
      exit -1
    fi

    # -v  verbose
    # -bb build binary
    # The --define sets the build directory
    rpmbuild -v -bb $package_name.spec --define "_topdir ${PWD}/$BUILD_DIR"
    # Copy built package to target directory
    mkdir -p ./$RPM_TARGET_DIR
    cp ./$BUILD_DIR/RPMS/noarch/$package_name*.rpm ./$RPM_TARGET_DIR
    rm -rf ./$BUILD_DIR
}

function check_if_package_exists() {
    local package_name=$1
    log "Checking if package $package_name does exist"
    ls ./$RPM_TARGET_DIR/$package_name*.noarch.rpm 1> /dev/null 2>&1
    if ! [ $? -eq 0 ]; then
      log "Build unsuccessful: The package $package_name does not exist!"
      # Return a non-zero value indicating that the rpm does not exist
      exit -1
    fi
    # In case everything went fine
    log "Build of package $package_name successfully finished!"
    return 0
}

# Only upload the packages to any repository in case we have a release build,
# i.e. a build that is not a gerrit build, but has been triggered by a gerrit
# submit.
# To which repository the package will be uploaded in the end is decided by
# looking at the version of the built rpm.
function upload_packages_to_repository() {

  # not a release, don't upload any packages
  if [[ $RELEASE -ne 1 ]] ; then
    return 0
  fi

  # determine the type of release based on the RPMs (already verified to exist)
  local is_dev=0
  local is_staging=0
  for rpm in `find ./$RPM_TARGET_DIR -name "*.rpm"` ; do
    # check the release tag .el6 for staging or <number>.el6 for dev
    [[ "`rpm -qp --queryformat '%{RELEASE}' $rpm`" == \.* ]] && is_staging=1 || is_dev=1
  done

  # make sure we don't have a mix of dev and staging RPMs
  if [[ $is_dev -eq 1 && $is_staging -eq 1 ]] ; then
    echo "Build unsuccessful: Mismatched dev/staging RPM versions!"
    exit -1
  elif [[ $is_dev -eq 0 && $is_staging -eq 0 ]] ; then
    echo "Build unsuccessful: Could not determine dev or staging release from RPM versions!"
    exit -1
  fi

  # pick the correct upload repository
  upload_repository=$TESTING_REPOSITORY
  [[ $is_staging -eq 1 ]] && upload_repository=$RELEASE_REPOSITORY

  # Upload the packages which have just been built
  # For a single package one can also use the command:
  #   upload2pulp -r neurorobotics -f my_package.rpm
  #
  # This command is provided by infra, also see:
  # https://bbpteam.epfl.ch/project/issues/servicedesk/customer/portal/3/HELP-3705
  log "Uploading the packages to the repository now ..."
  log "Command: 'upload2pulp -r $upload_repository -d ./$RPM_TARGET_DIR'"
  upload2pulp -r $upload_repository -d ./$RPM_TARGET_DIR
  if ! [ $? -eq 0 ]; then
    log "Build unsuccessful: Uploading packages did not work!"
    # Return a non-zero value indicating that uploading did not work
    exit -1
  fi
}


################################################################################
# Main program
################################################################################
#
clean_up
build_rpm_package $NEUROROBOTICS_SERVER_SCRIPTS_PKG_NAME
build_rpm_package $NEUROROBOTICS_ROS_SCRIPTS_PKG_NAME
#
# After we have built the package we explicitly check if the rpm packages do
# exist now (since we clean at the beginning, they can only be the new ones).
check_if_package_exists $NEUROROBOTICS_SERVER_SCRIPTS_PKG_NAME && check_if_package_exists $NEUROROBOTICS_ROS_SCRIPTS_PKG_NAME && upload_packages_to_repository