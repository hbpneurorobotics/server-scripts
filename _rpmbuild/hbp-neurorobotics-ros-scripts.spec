Summary: Scripts to start / stop ROS and ROS bridge for the Neurorobotics Platform (NRP)
Name: hbp-neurorobotics-ros-scripts
Version: 1.3.0
Release: %{?dist}
BuildArch: noarch
License: GPL

%description
This package contains all the scripts used to manage the following component of the NRP: ROS and ROS bridge.

%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/opt/bbp/ros
cp ../../../ros/* %{buildroot}/opt/bbp/ros
chmod -R o+rx %{buildroot}/opt/bbp/ros

%files
/opt/bbp/ros
