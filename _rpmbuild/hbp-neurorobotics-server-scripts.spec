Summary: Scripts to start / stop the HBP Neurorobotics Platform (NRP) (simulation-factory and REST services)
Name: hbp-neurorobotics-server-scripts
Version: 1.3.0
Release: %{?dist}
BuildArch: noarch
License: GPL

%description
This package contains all the scripts used to manage the following component of the NRP: simulation-factory and REST services.

%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/opt/bbp/nrp-services
cp ../../../nrp-services/* %{buildroot}/opt/bbp/nrp-services
chmod -R o+rx %{buildroot}/opt/bbp/nrp-services

%files
/opt/bbp/nrp-services
