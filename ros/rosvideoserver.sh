#!/bin/sh

# Loads all the modules and set all needed environment variables
DIR="$( cd "$( dirname "$0" )" && pwd )"
. $DIR/rosenv.sh

# Kill any previous web_video_server instance
ps aux | grep 'web_video_server' | grep -v grep | awk '{print $2}' | xargs kill -9

# Start rosbridge
# Use exec here so that supervisord can start AND stop the process
exec rosrun web_video_server web_video_server _port:=$ROS_WEB_SERVER_PORT