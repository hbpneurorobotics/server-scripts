#!/bin/sh

# Loads all the modules and set all needed environment variables
DIR="$( cd "$( dirname "$0" )" && pwd )"
. $DIR/rosenv.sh

# Kill any previous roscore instance
ps aux | grep 'roscore\|rosmaster' | grep python | grep -v grep | awk '{print $2}' | xargs kill -9

# Should address network issue such as "Couldn't find an AF_INET address for ..."
export ROS_IP=`hostname -I | awk '{print $1}'`

# Start roscore
# Use exec here so that supervisord can start AND stop the process
exec roscore
