#!/bin/sh

# Loads all the modules and set all needed environment variables
DIR="$( cd "$( dirname "$0" )" && pwd )"
. $DIR/rosenv.sh

# Kill any previous rosbridge instance
ps aux | grep 'rosbridge_websocket' | grep python | grep -v grep | awk '{print $2}' | xargs kill -9

# Start rosbridge
# Use exec here so that supervisord can start AND stop the process
exec roslaunch rosbridge_server rosbridge_websocket.launch
